﻿using FluentValidation;
using TodoApi.Models;

public class IdNumberValidator : AbstractValidator<IdNumber>
{
    public IdNumberValidator()
    {
        RuleFor(IdNumber => IdNumber.Id).NotNull().Length(13).Matches(@"^[0-9]+$");
    }
}
