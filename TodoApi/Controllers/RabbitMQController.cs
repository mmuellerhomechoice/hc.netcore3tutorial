﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using System.Text;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client.Core.DependencyInjection;


namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RabbitMQController : Controller
    {
        private readonly IQueueService _queueService;

        public RabbitMQController(IQueueService queueService)
        {
            _queueService = queueService;
        }

        // GET: api/RabbitMQ
        [HttpGet]
        [Route("[action]")]
        public string PostRabbitMqMessage(string str)
        {
            try
            {
                _queueService.Send(
                    @object: str,
                    exchangeName: "exchange.name",
                    routingKey: "routing.key");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return str;
        }
    }
}