﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;


namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        private readonly TodoContext _context;
        private readonly IValidator<IdNumber> _validator;

        public IdentityController(TodoContext context, IValidator<IdNumber> validator)
        {
            _context = context;
            _validator = validator;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<string>> ValidateIdNumber(string validateIdNumber)
        {
  
            IdNumber idNumber = new IdNumber();
            idNumber.Id = validateIdNumber;

            var results = _validator.Validate(idNumber);

            if (!results.IsValid)
            {
                return "not valid";
            }
            return "valid";
        }
    }
}